﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Angular.ecommerce.Models;

namespace Angular.ecommerce.ItemServices
{
    public class ItemQuery : IItemQuery
    {
        public List<Item> Items { get; set; }

        public ItemQuery()
        {
            Items = new List<Item>();
            Item item1 = new Item(1, "Beemer Jacket", "This is the Beemer Jacket Description", 30);
            Item item2 = new Item(2, "Snow Patrol Down Jacket", "This is the Snow Patrol Down Jacket Description", 70);
            Item item3 = new Item(3, "Apparition Jacket", "This is the Apparition Jacket Description", 70);
            Item item4 = new Item(4, "Sitzmark Jacket", "This is the Sitzmark Jacket Description", 65);
            Item item5 = new Item(5, "Wind Jammer Hooded Jacket", "This is the Wind Jammer Hooded Jacket Description", 50);
            Item item6 = new Item(6, "Heart Breaker Baseball Cap", "This is the Heart Breaker Baseball Cap Description", 15);

            Items.Add(item1);
            Items.Add(item2);
            Items.Add(item3);
            Items.Add(item4);
            Items.Add(item5);
            Items.Add(item6);

            Image item1image1 = new Image("Apparition-Jacket-1.jpg");
            Image item1image2 = new Image("Apparition-Jacket-2.jpg");
            item3.AddImage(item1image1);
            item3.AddImage(item1image2);

            Image item2image1 = new Image("Beemer-Jacket-1.jpg");
            Image item2image2 = new Image("Beemer-Jacket-2.jpg");
            item1.AddImage(item2image1);
            item1.AddImage(item2image2);

            Image item3image1 = new Image("Heart-Breaker-Baseball-1.jpg");
            Image item3image2 = new Image("Heart-Breaker-Baseball-2.jpg");
            item6.AddImage(item3image1);
            item6.AddImage(item3image2);

            Image item4image1 = new Image("Sitzmark-Jacket-1.jpg");
            Image item4image2 = new Image("Sitzmark-Jacket-2.jpg");
            item4.AddImage(item4image1);
            item4.AddImage(item4image2);

            Image item5image1 = new Image("Snow-Patrol-Down-Jacket-1.jpg");
            Image item5image2 = new Image("Snow-Patrol-Down-Jacket-2.jpg");
            item2.AddImage(item5image1);
            item2.AddImage(item5image2);

            Image item6image1 = new Image("Wind-Jammer-Hooded-Jacket-1.jpg");
            Image item6image2 = new Image("Wind-Jammer-Hooded-Jacket-1.jpg");
            item5.AddImage(item6image1);
            item5.AddImage(item6image2);
        }

        public List<Item> GetItems()
        {
            return Items;
        }



        public Item GetItemById(int id)
        {
            var item = from i in Items
                       where i.Id == id
                       select i;

            return item.FirstOrDefault();
        }
    }
}
