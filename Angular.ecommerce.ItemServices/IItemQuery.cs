﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Angular.ecommerce.Models;

namespace Angular.ecommerce.ItemServices
{
    public interface IItemQuery
    {
        List<Item> GetItems();
        Item GetItemById(int id);
    }
}
