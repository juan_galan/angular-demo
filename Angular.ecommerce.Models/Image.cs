﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angular.ecommerce.Models
{
    public class Image
    {
        public string URL { get; set; }
        public Image(string url)
        {
            URL = url;
        }
    }
}
