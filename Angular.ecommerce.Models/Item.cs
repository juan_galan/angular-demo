﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Angular.ecommerce.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public List<Image> Images { get; set; }
        public Item(int id, string displayName, string description, double price)
        {
            Id = id;
            DisplayName = displayName;
            Description = description;
            Price = price;
            Images = new List<Image>();
        }

        public void AddImage(Image image)
        {
            Images.Add(image);
        }
    }
}
