﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using Angular.ecommerce.ItemServices;
using Angular.ecommerce.Models;
using Newtonsoft.Json;

namespace Angular_ecommerce.Controllers
{
    public class ItemsController : ApiController
    {
        public List<Item> Get()
        {
            IItemQuery queryService = new ItemQuery();
            List<Item> items = queryService.GetItems();

            return items;
        }

        public Item Get(int id)
        {
            IItemQuery queryService = new ItemQuery();
            Item item = queryService.GetItemById(id);

            return item;
        }

    }
}
