﻿(function () {
	'use strict';

	angular
        .module('App.Items.Service', ['ngResource'])
        .factory('Items.Service', dataservice);

	dataservice.$inject = ['$resource'];
	
	function dataservice($resource)
	{
		return $resource('/api/items', {}, {
			query:
			{
				method: 'GET', params: {}, isArray: true
			}
		})
	}

})();


