﻿(function () {
    'use strict';

    angular
    .module('App.Items.Controller', ['App.Items.Service'])
    .controller('Items.Controller.List', ItemsList)
    .controller('Items.Controller.Details', ItemDetails);

    ItemsList.$inject = ['Items.Service', 'imageUrl', 'underscore'];
    ItemDetails.$inject = ['Items.Service', 'imageUrl', 'underscore', '$routeParams'];

    function ItemsList(ItemsService, imageUrl, _) {
        var vm = this;
        vm.items = [];

        ItemsService.query({}, function (result) {
            vm.items = result;
        });

        vm.getItemFirstImage = function(item)
        {
            var firstImage = _.first(item.Images);

            return imageUrl + '/' + firstImage.URL;
        }
    }

    function ItemDetails(ItemsService, imageUrl, _, routeParams)
    {
        var vm = this;
        vm.item = {};

        ItemsService.get({ id: routeParams.itemId }, function (item) {
            vm.item = item;
        });

        vm.getImageSrc = function(image)
        {
            return imageUrl + '/' + image.URL;
        }

        vm.initSlider = function(last)
        {
            if (last && !vm.sliderStarted)
            {
                vm.sliderStarted = true;
                console.log('init slider');
                jQuery('.bxslider').bxSlider();
            }
        }

        vm.addToCart = function(quantity, item)
        {
            alert('Added ' + quantity + ' of ' + item.DisplayName + ' to the cart ');
        }

    }

})();


