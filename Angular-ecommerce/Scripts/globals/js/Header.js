﻿(function () {
    'use strict';

    angular
        .module('App.Header', [])
        .controller('Header.Controller', HeaderController);

    HeaderController.$inject = ['header'];

    function HeaderController(headerConfig)
    {
        var header = this;
        header.config = headerConfig;
    }

})();


