﻿(function () {
    'use strict';

    angular
        .module('App', ['ngRoute', 'App.Items.Service', 'App.Items.Controller', 'App.Header', 'App.Footer'])
        .constant('header',
        {
            title: 'Angular eCommerce',
            leftTabs: [
                {
                    name: 'Category 1',
                    route: '#/items'
                },
                {
                    name: 'Category 2',
                    route: '#/items'
                }
            ],
            rightTabs: [
                {
                    name: 'Sign In',
                    route: '#'
                },
                {
                    name: 'Register',
                    route: '#'
                }
            ]
        })
        .constant('imageUrl', '/Content/products/')
        .constant('underscore', _)
        .config(config);

    function config($routeProvider) {
        $routeProvider
            .when('/items', {
                templateUrl: '/Scripts/items/templates/itemlist.html',
                controller: 'Items.Controller.List',
                controllerAs: 'view'
            }).
          when('/items/:itemId', {
              templateUrl: '/Scripts/items/templates/itemdetails.html',
              controller: 'Items.Controller.Details',
              controllerAs: 'view'
          }).
          otherwise({
              redirectTo: '/items'
          });
    };

})();





